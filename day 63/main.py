from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy import Integer, String, Float
import sqlalchemy

'''
Red underlines? Install the required packages first: 
Open the Terminal in PyCharm (bottom left). 

On Windows type:
python -m pip install -r requirements.txt

On MacOS type:
pip3 install -r requirements.txt

This will install the packages from requirements.txt for this project.
'''

app = Flask(__name__)


class Base(DeclarativeBase):
    pass


app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///books.db"
db = SQLAlchemy(model_class=Base)
db.init_app(app)


class Books(db.Model):
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    title: Mapped[str] = mapped_column(String(250), unique=True, nullable=False)
    author: Mapped[str] = mapped_column(String(250), nullable=False)
    rating: Mapped[float] = mapped_column(Float, nullable=False)


@app.route('/')
def home():
    with app.app_context():
        books = db.session.execute(db.select(Books)).scalars()
        book_count = db.session.query(Books).count()
        if book_count == 0:
            books = []
        return render_template('index.html', books=books)


@app.route('/<item_id>/remove')
def delete(item_id):
    with app.app_context():
        book_to_delete = db.session.execute(db.select(Books).where(Books.id == item_id)).scalar()
        db.session.delete(book_to_delete)
        db.session.commit()
    return redirect("/")


@app.route('/<item_id>/edit', methods=['GET', 'POST'])
def edit(item_id):
    if request.method == 'POST':
        with app.app_context():
            book_to_edit = db.session.execute(db.select(Books).where(Books.id == item_id)).scalar()
            book_to_edit.rating = request.form['newRating']
            db.session.commit()
        return redirect('/')
    else:
        with app.app_context():
            book_to_edit = db.session.execute(db.select(Books).where(Books.id == item_id)).scalar()
        return render_template('edit.html', book=book_to_edit, item_id=item_id)


# @app.route('/data', methods=['GET', 'POST'])
# def handle_data():
#     if request.method == 'POST':
#         with app.app_context():
#             book = Books(
#                 title=request.form['bookName'],
#                 author=request.form['bookAuthor'],
#                 rating=request.form['bookRating']
#             )
#
#             db.session.add(book)
#             db.session.commit()
#     return render_template('add.html')


@app.route('/add', methods=['GET', 'POST'])
def add():
    data = ''
    if request.method == 'POST':
        try:
            data = 'Added'
            with app.app_context():
                book = Books(
                    title=request.form['bookName'],
                    author=request.form['bookAuthor'],
                    rating=request.form['bookRating']
                )

                db.session.add(book)
                db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            data = 'Error: Book already in library'

    return render_template('add.html', data=data)


if __name__ == "__main__":
    app.run(debug=True)

with app.app_context():
    db.create_all()
